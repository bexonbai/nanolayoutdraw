//
//  LPLayout.swift
//  NanoLayoutDraw
//
//  Created by Bexon Pak on 2022/2/16.
//
import Foundation

// MARK: - LPLayout
struct LPLayout: Codable {
    let name, serialNo, manufacturer, firmwareVersion: String
    let hardwareVersion, model: String
    let discovery: Discovery
    let effects: Effects
    let firmwareUpgrade: Discovery
    let panelLayout: PanelLayout
    let qkihnokomhartlnp, schedules: Discovery
}

// MARK: - Discovery
struct Discovery: Codable {
}

// MARK: - Effects
struct Effects: Codable {
    let effectsList: [String]
    let select: String
}

// MARK: - PanelLayout
struct PanelLayout: Codable {
    let globalOrientation: GlobalOrientation
    let layout: Layout
}

// MARK: - GlobalOrientation
struct GlobalOrientation: Codable {
    let value, max, min: Int
}

// MARK: - Layout
struct Layout: Codable {
    let numPanels, sideLength: Int
    let positionData: [PositionDatum]
}

// MARK: - PositionDatum
struct PositionDatum: Codable {
    let panelID, x, y, o: Int
    let shapeType: Int

    enum CodingKeys: String, CodingKey {
        case panelID = "panelId"
        case x, y, o, shapeType
    }
}

// MARK: - On
struct On: Codable {
    let value: Bool
}
