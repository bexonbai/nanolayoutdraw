//
//  NanoLayoutDrawApp.swift
//  NanoLayoutDraw
//
//  Created by Bexon Pak on 2022/1/12.
//

import SwiftUI

@main
struct NanoLayoutDrawApp: App {

  var body: some Scene {
    WindowGroup {
      ContentView()
    }
    
    WindowGroup("Color Picker") {
      ColorPickerView()
    }.handlesExternalEvents(matching: Set(arrayLiteral: "ColorPickerWindow"))
      .commands {
        CommandMenu("Tools") {
          Button("Color Picker") {
            OpenWindows.ColorPickerView.open()
          }
          .keyboardShortcut("c", modifiers: .option)
        }
      }
  }
}

