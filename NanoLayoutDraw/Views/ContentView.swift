//
//  ContentView.swift
//  NanoLayoutDraw
//
//  Created by Bexon Pak on 2022/1/12.
//

import SwiftUI

struct ContentView: View {
  @Environment(\.colorScheme) var colorScheme
  @State var findByID: String = ""
  @StateObject var viewModel = MainViewModel()
  @State var showPanelId = false
  @State private var currentPosition: CGSize = .zero
  @State private var newPosition: CGSize = .zero
  
  var body: some View {
    HStack{
      VStack{
        HStack {
          Text("Layout Data String:").padding(.top, 10).padding(.horizontal, 10)
          Spacer()
          
          HStack {
            Text("Find: ")
            TextField("Panel ID", text: $findByID)
            Button {
              viewModel.layoutString = viewModel.layoutString
            } label: {
              Text("Find")
            }
          }.frame(width: 200, alignment: .leading).padding(.top, 10).padding(.horizontal, 10)
          
        }
        TextEditor(text: $viewModel.layoutString).frame(height: 50, alignment: .leading)
          .frame(maxHeight: 50)
          .border(.tertiary, width: 1)
          .padding(.horizontal, 10)
        HStack {
          Button("Clear") {
            self.viewModel.layoutString = ""
          }
          Toggle("Show IDs", isOn: $showPanelId)
            .toggleStyle(.checkbox)
          Spacer()
          Button("Reset") {
            currentPosition = CGSize(width: 0, height: 0)
            newPosition = CGSize(width: 0, height: 0)
          }
        }.padding(.horizontal, 10)
        Spacer()
        VStack {
          Canvas { context, size in
            viewModel.drawingFrameWidth = Int(size.width)
            viewModel.drawingFrameHeight = Int(size.height)
            context.withCGContext { cgContext in
              viewModel.panelVerticesList.forEach { panelVertices in
                if(showPanelId) {
                  if findByID != "" {
                    if (String(panelVertices.panelId) == findByID) {
                      let text = Text(String(panelVertices.panelId)).foregroundColor(Color.red)
                      context.blendMode = GraphicsContext.BlendMode.softLight
                      context.draw(text, at: panelVertices.centroid)
                    }
                  } else {
                    let text = Text(String(panelVertices.panelId))
                    context.blendMode = GraphicsContext.BlendMode.softLight
                    context.draw(text, at: panelVertices.centroid)
                  }
                }
                var f = true
                panelVertices.panelVertices.forEach { v in
                  if(f) {
                    f = !f
                    cgContext.move(to: v)
                    if(colorScheme == .dark) {
                      cgContext.setStrokeColor(.white)
                    } else {
                      cgContext.setStrokeColor(.black)
                    }
                    if (String(panelVertices.panelId) == findByID) {
                      cgContext.setStrokeColor(CGColor.init(red: 255, green: 0, blue: 0, alpha: 1))
                    }
                    cgContext.setFillColor(.clear)
                    cgContext.setLineWidth(1)
                  } else {
                    cgContext.addLine(to: v)
                  }
                }
                cgContext.closePath()
                cgContext.drawPath(using: CGPathDrawingMode.eoFillStroke)
              }
            }
          }
          .offset(x: self.currentPosition.width, y: self.currentPosition.height)
          .clipped()
        }
        .background(.background)
        .border(.tertiary, width: 1)
        .padding(10)
        .gesture(
          DragGesture()
            .onChanged { value in
              self.currentPosition = CGSize(width: value.translation.width + self.newPosition.width, height: value.translation.height + self.newPosition.height)
            }.onEnded { value in
              self.currentPosition = CGSize(width: value.translation.width + self.newPosition.width, height: value.translation.height + self.newPosition.height)
              self.newPosition = self.currentPosition
            })
      }
    }
    .frame(minWidth: 450, minHeight: 600, alignment: .center)
    
  }
}
