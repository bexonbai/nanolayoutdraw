//
//  ColorPickerView.swift
//  NanoLayoutDraw
//
//  Created by Bexon Pak on 2022/1/14.
//

import SwiftUI

struct ColorPickerView: View {
  @State private var bgColor = Color(.sRGB, red: 0.98, green: 0.9, blue: 0.2)
  @State private var kelvin = "0"
  var body: some View {
    VStack {
      ColorPicker("Select Color", selection: $bgColor)
      HStack {
        Text("Hex: ")
        Text(bgColor.hex)
      }
      HStack {
        Text("Kelvin: ")
        TextField("kelvin", text: $kelvin)
          .onChange(of: kelvin) { newValue in
            bgColor = Color(kelvinString: newValue)
          }
      }
      Spacer()
        .padding()
    }
    .frame(minWidth: 200, minHeight: 180, alignment: .center)
  }
}

struct ColorPickerView_Previews: PreviewProvider {
  static var previews: some View {
    ColorPickerView()
  }
}
