//
//  OpenWindow.swift
//  NanoLayoutDraw
//
//  Created by Bexon Pak on 2022/1/14.
//

import SwiftUI

enum OpenWindows: String, CaseIterable {
  case ColorPickerView = "ColorPickerWindow"
  
  func open(){
    if let url = URL(string: "nanolayoutdraw://\(self.rawValue)") {
      NSWorkspace.shared.open(url)
    }
  }
}
