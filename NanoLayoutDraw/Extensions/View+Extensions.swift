//
//  View+Extensions.swift
//  NanoLayoutDraw
//
//  Created by Bexon Pak on 2022/1/14.
//

import SwiftUI
import Foundation

extension View {
  @discardableResult
  func openInWindow(title: String, sender: Any?) -> NSWindow {
    let controller = NSHostingController(rootView: self)
    let win = NSWindow(contentViewController: controller)
    win.contentViewController = controller
    win.title = title
    win.makeKeyAndOrderFront(sender)
    return win
  }
}
