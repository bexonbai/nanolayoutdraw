//
//  Color+Extensions.swift
//  NanoLayoutDraw
//
//  Created by Bexon Pak on 2022/1/14.
//

import SwiftUI

extension Color {
  
  init(kelvinString: String) {
    guard let kelvin = Double(kelvinString) else { self.init(red: 0, green: 0, blue: 0); return }
    var temp: Double
    var r: Double
    var g: Double
    var b: Double
    let newKelvin = kelvin / 100.0
    if (newKelvin <= 66.0) {
      r = 255.0
    } else {
      temp = newKelvin - 60.0
      temp = 329.698727446 * pow(temp, -0.1332047592)
      r = clamp(x:temp, a:0.0, b:255.0)
    }
    if (newKelvin <= 66.0) {
      temp = newKelvin
      temp = 99.4708025861 * log(temp) - 161.1195681661
      g = clamp(x: temp, a: 0.0, b: 255.0)
    } else {
      temp = newKelvin - 60.0
      temp = 288.1221695283 * pow(temp, -0.0755148492)
      g = clamp(x: temp, a: 0.0, b: 255.0)
    }
    if (newKelvin >= 66.0) {
      b = 255.0
    } else if (newKelvin <= 19.0) {
      b = 0.0
    } else {
      temp = newKelvin - 10.0
      temp = 138.5177312231 * log(temp) - 305.0447927307
      b = clamp(x: temp, a: 0.0, b: 255.0)
    }
    print(r, g, b)
    self.init(.sRGB, red: r, green: g, blue: b)
  }
  
  var hex: String {
    let colorString = "\(self)"
    var hex = ""
    let colorArray: [String] = colorString.components(separatedBy: " ")
    if colorArray.count > 1 {
      var r: CGFloat = CGFloat((Float(colorArray[1]) ?? 1))
      var g: CGFloat = CGFloat((Float(colorArray[2]) ?? 1))
      var b: CGFloat = CGFloat((Float(colorArray[3]) ?? 1))
      
      if (r < 0.0) {r = 0.0}
      if (g < 0.0) {g = 0.0}
      if (b < 0.0) {b = 0.0}
      
      if (r > 1.0) {r = 1.0}
      if (g > 1.0) {g = 1.0}
      if (b > 1.0) {b = 1.0}
      
      let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
      hex = String(format: "#%06X", rgb)
    }
    return hex
  }
}

func clamp(x: Double, a: Double, b: Double) -> Double {
  if (x < a) { return a } else if (x > b) { return b } else { return x }
}
