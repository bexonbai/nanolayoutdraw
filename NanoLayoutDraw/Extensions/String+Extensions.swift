//
//  String+Extensions.swift
//  NanoLayoutDraw
//
//  Created by Bexon Pak on 2022/1/17.
//

import Foundation

extension String {
    var isBlank: Bool {
        return allSatisfy({ $0.isWhitespace })
    }
  
  func separate(every stride: Int = 2, with separator: Character = " ") -> String {
    return String(enumerated().map { $0 > 0 && $0 % stride == 0 ? [separator, $1] : [$1]}.joined())
  }
}
